﻿/************************************************************************************************************
* file    : DefaultController.cs
* author  : LTBLV12374IVQQ5
* function: 
* history : created by LTBLV12374IVQQ5 2019/7/2 16:57:17
************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using IStrong.EC.Web.Controllers;
using IStrong.EC.WebApi.Demo01.Entity;
using IStrong.EC.WebApi.Demo01.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace IStrong.EC.WebApi.Demo01.Controllers
{
    /// <summary>
    /// Api控制器模板
    /// </summary>
    [Route("api/default")]
    [ApiController]
    public class DefaultController : BaseController
    {
        /// <summary>
        /// 日志
        /// </summary>
        private readonly ILogger<DefaultController> _logger;

        /// <summary>
        /// 配置
        /// </summary>
        private readonly IConfiguration _config;
        /// <summary>
        /// 
        /// </summary>
        private readonly DefaultRepository _repository;

        /// <summary>
        /// 注入服务
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="config"></param>
        /// <param name="repository"></param>
        public DefaultController(ILogger<DefaultController> logger, IConfiguration config, DefaultRepository repository)
        {
            _logger = logger;
            _config = config;
            _repository = repository;
        }

        /// <summary>
        /// GET api/default
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Obsolete]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
            

        }
        /// <summary>
        /// 测试排序
        /// </summary>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet("{sort}")]
        [Obsolete]
        public ActionResult<IEnumerable<string>> Get(string sort)
        {
            return new string[] { "value1", "value2" };
            
        }

        /// <summary>
        /// GET api/default/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";

        }

        /// <summary>
        /// POST api/default
        /// 用户添加数据
        /// </summary>
        [HttpPost]
        public void Post([FromBody] string value)
        {
            
        }

        /// <summary>
        /// PUT api/default/5
        /// 用于修改数据
        /// </summary>
        /// <param name="adcd"></param>
        /// <param name="value"></param>
        [HttpPut]
        public void Put(int adcd, [FromBody] string value)
        {
            
        }

        /// <summary>
        /// DELETE api/default/5
        /// 用于删除数据
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
