﻿/************************************************************************************************************
* file    : DefaultController.cs
* author  : LTBLV12374IVQQ5
* function: 
* history : created by LTBLV12374IVQQ5 2019/7/3 15:50:11
************************************************************************************************************/
using IStrong.EC.WebApi.Area.Repository;
using IStrong.EC.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using IStrong.EC.WebApi.Area.Entity;

namespace IStrong.EC.WebApi.Area.Controller
{
    /// <summary>
    /// Api控制器模板
    /// </summary>
    [Route("ebu/api/v1/sjz/data/execute")]
    [ApiController]
    public class SsAreaController : BaseController
    {
        /// <summary>
        /// 日志
        /// </summary>
        private readonly ILogger<SsAreaController> _logger;

        /// <summary>
        /// 配置
        /// </summary>
        private readonly IConfiguration _config;

        private readonly SsAreaRepository _repository;

        /// <summary>
        /// 注入服务
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="config"></param>
        /// <param name="repository"></param>
        public SsAreaController(ILogger<SsAreaController> logger, IConfiguration config, SsAreaRepository repository)
        {
            _logger = logger;
            _config = config;
            _repository = repository;
        }

         /// <summary>
         /// GET api/values
         /// </summary>
         /// <returns></returns>
         [HttpGet]
         [Obsolete]
         public ActionResult<IEnumerable<string>> Get()
         {
             var list = _repository.GetAreaInfo(null);
             return Json(list);
         }
         
        /// <summary>
        /// POST
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="ssArea"></param>
        [HttpPost("execute")]
        [Obsolete]
         public ActionResult<string> Post(string mappingId, [FromBody] SsArea ssArea)
        {
            if (mappingId == "SsArea.i_ss_area")
            {
                return Json(_repository.InsertAreaInfo(ssArea));
            }
            if (mappingId == "SsArea.u_ss_area")
            {
                return Json(_repository.UpdateAreaInfo(ssArea));
            }
            if (mappingId == "SsArea.d_ss_area")
            {
                return Json(_repository.DeleteAreaInfo(ssArea.Adcd));
            }
            return Json("无效mappingId");

        }

        /// <summary>
        /// PUT api/values/5
        /// </summary>
        [HttpPut]
        [Obsolete]
        public void Put([FromBody] string value)
        {
            
        }

        /// <summary>
        /// DELETE api/values/5
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("id")]
        public void Delete(int id)
        {

        }
       
    }
}
