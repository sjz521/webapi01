﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using IStrong.EC.Abstractions.Interfaces;

namespace IStrong.EC.WebApi.Area.Entity
{
	/// <summary> 
	/// 行政区划信息
	/// </summary> 
	public class SsArea:IEntity
	{
		/// <summary> 
		/// 排序
		/// </summary> 
		public Decimal? OrderId{ get; set; }

		/// <summary> 
		/// 面积(平方公里)
		/// </summary> 
		public Decimal? Area{ get; set; }

		/// <summary> 
		/// 人口(人)
		/// </summary> 
		public Decimal? Popunum{ get; set; }

		/// <summary> 
		/// 位置描述(东南西北所临)
		/// </summary> 
		public string Location{ get; set; }

		/// <summary> 
		/// 防汛任务情况
		/// </summary> 
		public string Floodtask{ get; set; }

		/// <summary> 
		/// 添加时间
		/// </summary> 
		public DateTime? Addtime{ get; set; }

		/// <summary> 
		/// 更新时间
		/// </summary> 
		public DateTime? Updtime{ get; set; }

		/// <summary> 
		/// 添加人
		/// </summary> 
		public Decimal? Adduserid{ get; set; }

		/// <summary> 
		/// 修改人
		/// </summary> 
		public Decimal? Upduserid{ get; set; }

		/// <summary> 
		/// 备注
		/// </summary> 
		public string Remark{ get; set; }

		/// <summary> 
		/// 简称
		/// </summary> 
		public string Shortname{ get; set; }

		/// <summary> 
		/// 内部地区编码（省市县6位，乡镇9位，村12位，自然村15位),内部编码规则3310XXYYYZZZ,xx,yyy,zzz取最大,倒序排
		/// </summary> 
		public string Adcd{ get; set; }

		/// <summary> 
		/// 地区名称
		/// </summary> 
		public string Adnm{ get; set; }

		/// <summary> 
		/// 经度
		/// </summary> 
		public Decimal? Lgtd{ get; set; }

		/// <summary> 
		/// 纬度
		/// </summary> 
		public Decimal? Lttd{ get; set; }

		/// <summary> 
		/// 最小经度
		/// </summary> 
		public Decimal? Minx{ get; set; }

		/// <summary> 
		/// 最小纬度
		/// </summary> 
		public Decimal? Miny{ get; set; }

		/// <summary> 
		/// 最大经度
		/// </summary> 
		public Decimal? Maxx{ get; set; }

		/// <summary> 
		/// 最大纬度
		/// </summary> 
		public Decimal? Maxy{ get; set; }

		/// <summary> 
		/// 联系人
		/// </summary> 
		public string Master{ get; set; }

		/// <summary> 
		/// 联系电话
		/// </summary> 
		public string Mobile{ get; set; }

		/// <summary> 
		/// 传真
		/// </summary> 
		public string Fax{ get; set; }

		/// <summary> 
		/// 是否沿海城市
		/// </summary> 
		public Decimal? Typhoonland{ get; set; }

		/// <summary> 
		/// 真实地区编码（省市县6位，乡镇9位，村12位，自然村15位）
		/// </summary> 
		public string Realadcd{ get; set; }

	}
}
