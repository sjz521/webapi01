﻿/************************************************************************************************************
* file    : SsAreaRepository.cs
* author  : LTBLV12374IVQQ5
* function: 
* history : created by LTBLV12374IVQQ5 2019/7/3 15:50:11
************************************************************************************************************/
using System;
using System.Collections.Generic;
using IStrong.EC.Abstractions.Interfaces;
using IStrong.EC.DAO.Abstractions.Interfaces;
using IStrong.EC.WebApi.Area.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace IStrong.EC.WebApi.Area.Repository
{
    /// <summary>
    /// 数据访问类
    /// </summary>
    public class SsAreaRepository : IService
    {
        /// <summary>
        /// 数据库操作组件
        /// </summary>
        private readonly IDbContext _db;

        /// <summary>
        /// 配置信息
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// 日志组件
        /// </summary>
        private readonly ILogger<SsAreaRepository> _logger;

        /// <summary>
        /// 注入服务
        /// </summary>
        /// <param name="db"></param>
        /// <param name="configuration"></param>
        /// <param name="logger"></param>
        public SsAreaRepository(IDbContext db, ILogger<SsAreaRepository> logger, IConfiguration configuration)
        {
            _db = db;
            _logger = logger;
            _configuration = configuration;
        }


        /// <summary>
        /// 获取信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public IEnumerable<SsArea> GetAreaInfo(string orderId = null)
        {
            return _db.Query<SsArea>("SsArea.s_ss_area", new { orderId });
        }
        /// <summary>
        /// 测试插入数据
        /// </summary>
        public SsArea InsertAreaInfo(SsArea ssArea)
        {
            _db.Execute("SsArea.i_ss_area", ssArea);//返回影响行数
            return ssArea;
        }
        /// <summary>
        /// 测试更新
        /// </summary>
        /// <returns></returns>
        public SsArea UpdateAreaInfo(SsArea ssArea)
        {
            _db.Execute("SsArea.u_ss_area", ssArea);
            return ssArea;
        }
        /// <summary>
        /// 测试删除数据，并将要删除的数据返回
        /// </summary>
        /// <param name="adcd"></param>
        /// <returns></returns>
        public IEnumerable<SsArea> DeleteAreaInfo(string adcd = "331000000000")
        {
            var result = SearchAreaInfo(adcd);
            _db.Execute("SsArea.d_ss_area", new { ADCD = adcd });
            return result;
        }
        /// <summary>
        /// 输入关键词排序
        /// </summary>
        /// <param name="sort"></param>
        /// <returns></returns>
        public IEnumerable<SsArea> SortAreaInfo(string sort)
        {
            var list = _db.Query<SsArea>("SsArea.s_ss_area", sort);
            return list;
        }
        /// <summary>
        /// 根据adcd查询信息
        /// </summary>
        /// <param name="adcd"></param>
        /// <returns></returns>
        public IEnumerable<SsArea> SearchAreaInfo(string adcd)
        {
            return _db.Query<SsArea>("SsArea.s_ss_area", new { ADCD = adcd });
        }
    }
}
