##项目介绍
##WebApi的第1个Demo


##功能描述
##通过使用不同的mappingId路由实现了对数据的增删改查

##使用说明
##1.通过http://localhost:62776/ebu/api/v1/sjz/data/execute?mappingId=SsArea.s_ss_area访问数据库SS_AREA表的全部数据；
##2.通过http://localhost:62776/ebu/api/v1/sjz/data/execute?mappingId=SsArea.i_ss_area并以POST的方式进行访问。
##	在访问之前，以{"key1":value1,"key2":value2...}的格式将要插入的数据写在body的raw下面；
##3.通过http://localhost:62776/ebu/api/v1/sjz/data/execute?mappingId=SsArea.u_ss_area并以POST的方式进行访问，
##	就能将数据修改为写在body里面的内容
##4.通过http://localhost:62776/ebu/api/v1/sjz/data/execute?mappingId=SsArea.d_ss_area并以POST的方式进行访问，
##	且将要删除的数据的adcd写入body里面即可删除